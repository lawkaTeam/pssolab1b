
package nopattern;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

/*
 * 
 * This is the main class of the application. 
 */
public class GameOfLifeUI extends JFrame implements Observer {
	
	protected GameOfLife game;
    protected GameOfLifePanel view;
    
    protected JButton advanceButton;
    protected JPanel contentPane;
    protected final static int PAD_HEIGHT = 100;
    protected final static int PED_WIDTH = 50;
    
    protected final static String TITLE = "The Game of Life"; // TODO: title here?
    
    // Build the appropriate instance of the GameOfLifePanel class and initialize the
    //  application frame.
    public GameOfLifeUI(GameOfLife game) { // TODO: title as param?
        super(TITLE);
        game.addObserver(this);
        this.game = game;
        view = new GameOfLifePanel(game);
        advanceButton = new JButton("Advance");
        contentPane = new JPanel();
        initialize();
    }
    
    // Build the UI and set the "advance" button to cause the game object to 
    //  advance one generation.
    protected void initialize(){
        this.setSize(view.getWidth() + PED_WIDTH, view.getHeight() + PAD_HEIGHT);
        contentPane.setLayout(new FlowLayout());
        contentPane.add(view);
        contentPane.add(advanceButton);
        this.setContentPane(contentPane);
        this.setVisible(true);
        advanceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                game.advance();
            } 
        });
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}
}
